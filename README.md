[![build status](https://gitlab.com/axil/docker-archlinux/badges/master/build.svg)](https://gitlab.com/axil/docker-archlinux/commits/master)

Minimal Arch Linxu Docker image with `ruby`, `pandoc` and `texlive-bin` installed.
